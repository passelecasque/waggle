package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.com/catastrophic/assistance/ui"
)

type Artist struct {
	gorm.Model
	DeezerID int `gorm:"UNIQUE_INDEX"`
	ArtistID int
	Name     string `gorm:"UNIQUE"`
	// associations
	Groups []*Group `gorm:"many2many:group_artists;"`
}

func (a *Artist) String() string {
	if a.ArtistID != 0 {
		return fmt.Sprintf("%s:\n\thttps://www.deezer.com/artist/%d\n\t%s", ui.YellowUnderlined(a.Name), a.DeezerID, fmt.Sprintf("%s/artist.php?id=%d", config.URL, a.ArtistID))
	}
	return fmt.Sprintf("%s:\n\thttps://www.deezer.com/artist/%d", ui.YellowUnderlined(a.Name), a.DeezerID)
}
