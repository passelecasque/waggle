package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.com/catastrophic/assistance/ui"
)

const (
	noWEBFLACFlag     = "?"
	noPerfectFLACFlag = "!"
	forcedMatchFlag   = "*"

	deezerAlbumURL = "https://www.deezer.com/album/%d"
)

type Group struct {
	gorm.Model
	DeezerID          int `gorm:"UNIQUE_INDEX"`
	GroupID           int
	Matched           bool
	ForcedMatch       bool
	Ignore            bool
	NoWEBFLAC         bool
	NoPerfectFLAC     bool
	Name              string
	NumDiscs          int
	NumTracks         int
	DeezerReleaseType int
	ReleaseType       int
	ReleaseDate       string
	UserComment       string
	// associations
	Artists []*Artist `gorm:"many2many:group_artists;"`
}

func (g *Group) String() string {
	var noWEBFLAC, noPerfectFLAC, userComment, forcedMatch, releaseDate string
	if g.NoWEBFLAC {
		noWEBFLAC = ui.RedBold(noWEBFLACFlag)
	}
	if g.NoPerfectFLAC {
		noPerfectFLAC = ui.RedBold(noPerfectFLACFlag)
	}
	if g.ForcedMatch {
		forcedMatch = ui.RedBold(forcedMatchFlag)
	}
	if g.UserComment != "" {
		userComment = ui.RedBold(">> " + g.UserComment + " <<")
	}
	if IsReleaseDateInTheFuture(g.ReleaseDate) {
		releaseDate = ui.RedBold(g.ReleaseDate)
	} else {
		releaseDate = ui.BlueBold(g.ReleaseDate)
	}
	if g.Ignore {
		return fmt.Sprintf("%s | %-38s | %-52s | %2d | %s %s", releaseDate, fmt.Sprintf(deezerAlbumURL, g.DeezerID), ui.BlueDark("IGNORED"), g.NumTracks, ui.BlueDark(g.Name), userComment)
	}
	if g.Matched {
		return fmt.Sprintf("%s | %-38s | %-43s | %2d | %s%s%s%s %s", releaseDate, fmt.Sprintf(deezerAlbumURL, g.DeezerID), fmt.Sprintf("%s/torrents.php?id=%d", config.URL, g.GroupID), g.NumTracks, forcedMatch, noWEBFLAC, noPerfectFLAC, ui.Yellow(g.Name), userComment)
	}
	return fmt.Sprintf("%s | %-38s | %-54s | %2d | %s %s", releaseDate, fmt.Sprintf(deezerAlbumURL, g.DeezerID), ui.RedBold("UNMATCHED RELEASE"), g.NumTracks, ui.Yellow(g.Name), userComment)
}

func (g *Group) ShortString() string {
	return fmt.Sprintf("%s | %-38s | %2d | %s", ui.BlueBold(g.ReleaseDate), fmt.Sprintf(deezerAlbumURL, g.DeezerID), g.NumTracks, ui.Yellow(g.Name))
}

func (g *Group) ExportString() string {
	if g.Matched {
		return fmt.Sprintf("%-38s # %s | %2d | %s | %s", fmt.Sprintf(deezerAlbumURL, g.DeezerID), g.ReleaseDate, g.NumTracks, g.Name, fmt.Sprintf("%s/torrents.php?id=%d", config.URL, g.GroupID))
	}
	return fmt.Sprintf("%-38s # %s | %2d | %s", fmt.Sprintf(deezerAlbumURL, g.DeezerID), g.ReleaseDate, g.NumTracks, g.Name)
}

func (g *Group) ToCSV() []string {
	if g.UserComment != "" || g.Ignore || g.ForcedMatch {
		return []string{fmt.Sprintf("%d", g.DeezerID), fmt.Sprintf("%v", g.Ignore), g.UserComment, fmt.Sprintf("%v", g.ForcedMatch), fmt.Sprintf("%d", g.GroupID)}
	}
	return nil
}
