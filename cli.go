package main

import (
	"fmt"
	"strconv"

	"github.com/docopt/docopt-go"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
)

const (
	usage = `
	_ _ _ ____ ____ ____ _    ____ 
	| | | |__| | __ | __ |    |___ 
	|_|_| |  | |__] |__] |___ |___   (%s)
	
Description:
    Waggle dance to find sweet new releases from your favorite artists.
	
Usage:
    waggle check [<ARTIST>]
    waggle list (all|unmatched|ignored|missing-web|missing-perfect|artist <ARTIST>)
    waggle add-comment <DEEZER_ID> <COMMENT>
    waggle ignore <DEEZER_ID> [<COMMENT>]
    waggle match <DEEZER_ID> <TRACKER_ID>
    waggle unmatch <DEEZER_ID>
    waggle import (user-values|artists) <FILE>
    waggle export (user-values|artists|unmatched|missing-web|missing-perfect) <FILE>
    waggle remove <ARTIST>
    waggle show-config

Options:
    -h, --help             Show this screen.
    --version              Show version.
`
	fullName    = "waggle"
	fullVersion = "%s -- %s"
)

var Version = "dev"

func userAgent() string {
	return fullName + "/" + Version
}

type waggleArgs struct {
	builtin        bool
	showConfig     bool
	check          bool
	ignore         bool
	list           bool
	listAll        bool
	listIgnored    bool
	listArtist     bool
	match          bool
	unmatch        bool
	exportCmd      bool
	importCmd      bool
	remove         bool
	userValues     bool
	artistsList    bool
	unMatched      bool
	missingWEB     bool
	missingPerfect bool
	addComment     bool
	trackerID      int
	deezerID       int
	targetFile     string
	userComment    string
	artist         string
}

func (m *waggleArgs) parseCLI(osArgs []string) error {
	// parse arguments and options
	args, err := docopt.Parse(fmt.Sprintf(usage, Version), osArgs, true, fmt.Sprintf(fullVersion, fullName, Version), false, false)
	if err != nil {
		return errors.Wrap(err, "incorrect arguments")
	}
	if len(args) == 0 {
		// builtin command, nothing to do.
		m.builtin = true
		return nil
	}
	m.showConfig = args["show-config"].(bool)
	m.check = args["check"].(bool)
	m.list = args["list"].(bool)
	m.remove = args["remove"].(bool)
	if m.list {
		m.listAll = args["all"].(bool)
		m.listIgnored = args["ignored"].(bool)
		m.listArtist = args["artist"].(bool)
	}
	if m.check || m.listArtist || m.remove {
		// if not ok, it means the arg was optional
		m.artist, _ = args["<ARTIST>"].(string)
	}
	m.addComment = args["add-comment"].(bool)
	m.ignore = args["ignore"].(bool)
	m.match = args["match"].(bool)
	m.unmatch = args["unmatch"].(bool)
	if m.addComment || m.ignore || m.match || m.unmatch {
		deezerIDStr := args["<DEEZER_ID>"].(string)
		if deezerIDStr != "" {
			if deezerID, err := strconv.Atoi(deezerIDStr); err != nil {
				return errors.New("deezer ID must be an integer")
			} else {
				m.deezerID = deezerID
			}
		}
	}
	if m.match {
		trackerIDStr := args["<TRACKER_ID>"].(string)
		if trackerIDStr != "" {
			if trackerID, err := strconv.Atoi(trackerIDStr); err != nil {
				return errors.New("tracker ID must be an integer")
			} else {
				m.trackerID = trackerID
			}
		}
		if m.deezerID == 0 || m.trackerID == 0 {
			return errors.New("deezer ID and tracker ID are required")
		}
	}
	if m.addComment || m.ignore {
		m.userComment = args["<COMMENT>"].(string)
		if m.deezerID == 0 && m.userComment == "" {
			return errors.New("deezer ID and comment are required")
		}
	}
	m.exportCmd = args["export"].(bool)
	m.importCmd = args["import"].(bool)
	if m.exportCmd || m.importCmd {
		m.userValues = args["user-values"].(bool)
		m.artistsList = args["artists"].(bool)

		m.targetFile = args["<FILE>"].(string)
		if m.importCmd && !fs.FileExists(m.targetFile) {
			return errors.New("cannot find file")
		}
		if m.exportCmd && fs.FileExists(m.targetFile) {
			return errors.New("file already exists")
		}
	}
	if m.exportCmd || m.list {
		m.unMatched = args["unmatched"].(bool)
		m.missingWEB = args["missing-web"].(bool)
		m.missingPerfect = args["missing-perfect"].(bool)
	}
	return nil
}
