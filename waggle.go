package main

import (
	"encoding/csv"
	"html"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/deezer"
	"gitlab.com/catastrophic/assistance/intslice"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/catastrophic/assistance/strslice"
	"gitlab.com/catastrophic/assistance/ui"
	"gitlab.com/passelecasque/obstruction/tracker"
)

type WaggleDance struct {
	db             *gorm.DB
	tracker        *tracker.Gazelle
	trackerStarted bool
	d              *deezer.Deezer
}

func NewWaggleDance(dbFileName string, url, apikey, sid string) (*WaggleDance, error) {
	logthis.TimedInfo("Opening database.", logthis.VERBOSEST)
	db, err := gorm.Open("sqlite3", dbFileName)
	if err != nil {
		return nil, err
	}
	db.AutoMigrate(&Group{}, &Artist{})
	b, err := tracker.NewGazelle("gazelle_tracker", url, "", "", "", "", apikey, userAgent())
	if err != nil {
		return nil, err
	}
	d := deezer.New("", sid)
	return &WaggleDance{db: db, tracker: b, d: d}, nil
}

func (h *WaggleDance) Close() {
	if err := h.db.Close(); err != nil {
		logthis.TimedError(err, logthis.VERBOSEST)
	} else {
		logthis.TimedInfo("Database cleanly closed.", logthis.VERBOSEST)
	}
}

func (h *WaggleDance) AllArtists() ([]Artist, error) {
	var artists []Artist
	res := h.db.Find(&artists)
	return artists, res.Error
}

func (h *WaggleDance) ShowArtist(artist string, unmatchedOnly, ignoredOnly, missingPerfect, missingWeb bool) error {
	var aDB Artist
	var groups []Group
	// find artist with name in DB
	result := h.db.Where("name = ?", artist).First(&aDB)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return result.Error
	}
	switch {
	case unmatchedOnly:
		h.db.Model(&aDB).Order("release_date desc, name").Where("ignore = ?", false).Where("matched = ?", false).Related(&groups, "Groups")
	case ignoredOnly:
		h.db.Model(&aDB).Order("release_date desc, name").Where("ignore = ?", true).Related(&groups, "Groups")
	case missingPerfect:
		h.db.Model(&aDB).Order("release_date desc, name").Where("ignore = ?", false).Where("no_perfect_flac = ?", true).Related(&groups, "Groups")
	case missingWeb:
		h.db.Model(&aDB).Order("release_date desc, name").Where("ignore = ?", false).Where("no_webflac = ?", true).Related(&groups, "Groups")
	default:
		h.db.Model(&aDB).Order("release_date desc, name").Related(&groups, "Groups")
	}

	if len(groups) != 0 {
		logthis.Info(aDB.String()+"\n", logthis.NORMAL)
		for _, g := range groups {
			logthis.Info("\t"+g.String(), logthis.NORMAL)
		}
		logthis.Info("\n", logthis.NORMAL)
	}
	return nil
}

func (h *WaggleDance) RefreshArtist(artist string) error {
	id, err := h.RefreshDeezer(artist)
	if err != nil {
		return err
	}
	if !h.trackerStarted {
		h.tracker.StartRateLimiter()
		logthis.TimedInfo("Logging in.", logthis.VERBOSEST)
		if err := h.tracker.Login(); err != nil {
			return err
		}
		h.trackerStarted = true
	}
	return h.RefreshTracker(id)
}

func (h *WaggleDance) RefreshDeezer(artist string) (int, error) {
	logthis.TimedInfo(ui.BlueBold("Getting deezer info for artist ")+ui.RedBold(artist), logthis.NORMAL)
	a, isNewArtist, err := h.getArtist(artist)
	if err != nil {
		return -1, err
	}

	// getting artist discography
	discography, err := h.d.GetDiscographyInfo(a.DeezerID)
	if err != nil {
		return -1, err
	}
	// getting list of already known albums
	allKnownAlbumsIDs := h.getAlbumIDs(a.DeezerID)

	// parsing, refreshing and saving releases
	var allAlbumIDs []int
	for i := 0; i < int(discography.Results.Count); i++ {
		deezerID, _ := strconv.Atoi(discography.Results.Data[i].AlbID)
		// keeping track of all things found
		allAlbumIDs = append(allAlbumIDs, deezerID)

		var isNewRelease bool
		var g Group

		res := h.db.Where("deezer_id = ?", deezerID).First(&g)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			isNewRelease = true
			g.DeezerID = deezerID
		}
		g.Name = discography.Results.Data[i].AlbTitle
		g.ReleaseDate = discography.Results.Data[i].DigitalReleaseDate
		numDiscs, _ := strconv.Atoi(discography.Results.Data[i].NumberDisk)
		g.NumDiscs = numDiscs
		numTracks, _ := strconv.Atoi(discography.Results.Data[i].NumberTrack)
		g.NumTracks = numTracks
		switch discography.Results.Data[i].Type {
		case "0":
			g.DeezerReleaseType = deezer.Single
		case "1":
			g.DeezerReleaseType = deezer.Album
		case "2":
			g.DeezerReleaseType = deezer.Split
		case "3":
			g.DeezerReleaseType = deezer.EP
		default:
			g.DeezerReleaseType = deezer.UnknownType
		}
		// logging new releases
		if !isNewArtist && isNewRelease {
			logthis.TimedInfo(ui.RedBold("NEW RELEASE: ")+ui.YellowBold(g.ShortString()), logthis.NORMAL)
		}
		a.Groups = append(a.Groups, &g)
	}

	// checking if some releases have disappeared
	removedReleases := intslice.Difference(allKnownAlbumsIDs, allAlbumIDs)
	for _, rm := range removedReleases {
		var g Group
		res := h.db.Where("deezer_id = ?", rm).First(&g)
		if res.Error != nil {
			return -1, res.Error
		}
		logthis.TimedInfo("REMOVED RELEASE: "+ui.YellowBold(g.ShortString()), logthis.NORMAL)
		// removing from DB, keeping it does not seem to make sense.
		// in all likelihood, if a release comes back, it will have a new ID.
		res = h.db.Delete(&g)
		if res.Error != nil {
			return -1, res.Error
		}
	}

	// saving all
	res := h.db.Save(&a)
	return a.DeezerID, res.Error
}

func (h *WaggleDance) RefreshTracker(id int) error {
	var a Artist
	res := h.db.Where("deezer_id = ?", id).First(&a)
	if res.Error != nil {
		return res.Error
	}
	logthis.TimedInfo(ui.BlueBold("Getting tracker info for artist ")+ui.RedBold(a.Name), logthis.NORMAL)
	// finding associated groups
	var groups []Group
	h.db.Model(&a).Related(&groups, "Groups")

	var ga *tracker.GazelleArtist
	var err error
	if a.ArtistID == 0 {
		// look up artist on tracker
		ga, err = h.tracker.GetArtistFromName(a.Name)
		if err != nil {
			return err
		}
		logthis.TimedInfo("Found artist on tracker: "+ga.Name, logthis.VERBOSE)
		a.ArtistID = ga.ID
	} else {
		ga, err = h.tracker.GetArtist(a.ArtistID)
		if err != nil {
			return err
		}
	}

	for _, dbG := range groups {
		var matchFoundAgain bool
		for _, tg := range ga.Torrentgroup {
			groupName := html.UnescapeString(tg.GroupName)
			var editionNames []string
			for _, tt := range tg.Torrent {
				if tt.RemasterTitle != "" {
					editionNames = append(editionNames, tt.RemasterTitle)
				}
			}
			strslice.RemoveDuplicates(&editionNames)

			// refreshing existing match
			if dbG.Matched && dbG.GroupID == tg.GroupID {
				matchFoundAgain = true
				if tg.HasWEBFLAC() {
					logthis.TimedInfo("Existing match still exists for: "+groupName, logthis.VERBOSE)
					dbG.NoWEBFLAC = false
				} else {
					logthis.TimedInfo("Found match, but does not have WEB FLAC! "+groupName, logthis.VERBOSE)
					dbG.NoWEBFLAC = true
				}
				dbG.NoPerfectFLAC = !tg.HasPerfectFLAC()
				// TODO only save if changed
				res := h.db.Save(&dbG)
				if res.Error != nil {
					return res.Error
				}
				break
			}
			// unmatched, checking if possible to find one
			if !dbG.Matched && IsTheSameRelease(dbG.Name, groupName, editionNames) {
				var modified bool

				// saving match
				if dbG.GroupID == 0 {
					// trying to differentiate albums/EP/singles with the same title
					var foundApproximateMatch bool
					for _, tt := range tg.Torrent {
						// taking into account the fact that CD sources have more extra, non-track files
						if AboutTheSameNumberOfTracks(dbG.NumTracks, tt.FileCount, tt.IsWEBFLAC()) {
							foundApproximateMatch = true
							break
						}
					}
					if foundApproximateMatch {
						logthis.TimedInfo(ui.Yellow("Found NEW match for: ")+ui.Red(groupName), logthis.NORMAL)
						dbG.GroupID = tg.GroupID
						dbG.Matched = true
						modified = true
					}
				}
				// checking a match was just found
				if dbG.GroupID == tg.GroupID {
					dbG.NoPerfectFLAC = !tg.HasPerfectFLAC()
					dbG.NoWEBFLAC = !tg.HasWEBFLAC()
					modified = true
					if dbG.NoPerfectFLAC {
						if dbG.NoWEBFLAC {
							logthis.TimedInfo("Found match for "+groupName+", but the tracker is missing a WEB FLAC.", logthis.VERBOSE)
						} else {
							logthis.TimedInfo("Found match for "+groupName+", but the tracker is missing a perfect FLAC.", logthis.VERBOSE)
						}
					}
				}

				if modified {
					matchFoundAgain = true
					res := h.db.Save(&dbG)
					if res.Error != nil {
						return res.Error
					}
					break
				}
			}
		}

		if dbG.Matched && !matchFoundAgain {
			logthis.TimedInfo("Previous match cannot be found on tracker anymore. Unmatching "+dbG.Name, logthis.NORMAL)
			dbG.Matched = false
			dbG.ForcedMatch = false
			dbG.NoWEBFLAC = true
			dbG.NoPerfectFLAC = true
			dbG.GroupID = 0
			res := h.db.Save(&dbG)
			if res.Error != nil {
				return res.Error
			}
		}
	}

	// saving all
	return h.db.Save(&a).Error
}

func (h *WaggleDance) AddComment(deezerID int, comment string) error {
	var g Group
	res := h.db.Where("deezer_id = ?", deezerID).First(&g)
	if res.Error != nil {
		return res.Error
	}
	// TODO ask if overwrite if not null
	g.UserComment = comment
	return h.db.Save(&g).Error
}

func (h *WaggleDance) Ignore(deezerID int, userComment string) error {
	var g Group
	res := h.db.Where("deezer_id = ?", deezerID).First(&g)
	if res.Error != nil {
		return res.Error
	}
	g.Ignore = true
	if userComment != "" {
		g.UserComment = userComment
	}
	return h.db.Save(&g).Error
}

func (h *WaggleDance) UnMatch(deezerID int) error {
	var g Group
	res := h.db.Where("deezer_id = ?", deezerID).First(&g)
	if res.Error != nil {
		return res.Error
	}
	g.GroupID = 0
	g.Matched = false
	g.ForcedMatch = false
	return h.db.Save(&g).Error
}

func (h *WaggleDance) Match(deezerID, trackerID int) error {
	var g Group
	res := h.db.Where("deezer_id = ?", deezerID).First(&g)
	if res.Error != nil {
		return res.Error
	}
	g.GroupID = trackerID
	g.Matched = true
	g.ForcedMatch = true
	return h.db.Save(&g).Error
}

func (h *WaggleDance) ExportToCSV(filename string) error {
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	w := csv.NewWriter(f)
	if err := w.Write([]string{
		"deezerID",
		"ignored",
		"comment",
		"forcedMatch",
		"trackerID",
	}); err != nil {
		return err
	}

	// getting all groups
	var allGroups []Group
	res := h.db.Find(&allGroups)
	if res.Error != nil {
		return res.Error
	}

	for _, g := range allGroups {
		csvLine := g.ToCSV()
		if len(csvLine) != 0 {
			if err := w.Write(csvLine); err != nil {
				return err
			}
		}
	}
	w.Flush()
	return w.Error()
}

func (h *WaggleDance) ImportFromCSV(filename string) error {
	f, err := os.OpenFile(filename, os.O_RDONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	w := csv.NewReader(f)
	records, err := w.ReadAll()
	if err != nil {
		return err
	}
	for i, l := range records {
		if i == 0 {
			// headers
			continue
		}
		// parsing values
		deezerID := l[0]
		ignored, err := strconv.ParseBool(l[1])
		if err != nil {
			return err
		}
		userComment := l[2]
		forcedMatch, err := strconv.ParseBool(l[3])
		if err != nil {
			return err
		}
		trackerID, err := strconv.Atoi(l[4])
		if err != nil {
			return err
		}
		// finding the group in DB
		var g Group
		res := h.db.Where("deezer_id = ?", deezerID).First(&g)
		if errors.Is(res.Error, gorm.ErrRecordNotFound) {
			logthis.TimedError(errors.Wrap(res.Error, "could not find release with ID "+deezerID), logthis.NORMAL)
			continue
		}
		// updating values
		g.ForcedMatch = forcedMatch
		if forcedMatch {
			// only update if the match was made by user
			g.GroupID = trackerID
			g.Matched = true
		}
		g.Ignore = ignored
		g.UserComment = userComment
		// saving the group
		if res := h.db.Save(&g); res.Error != nil {
			return res.Error
		}
	}
	return nil
}

func (h *WaggleDance) ExportArtistList(filename string) error {
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	// getting all artists
	var artists []Artist
	res := h.db.Order("name collate nocase").Find(&artists)
	if res.Error != nil {
		return res.Error
	}

	for _, a := range artists {
		if _, err := f.WriteString(a.Name + "\n"); err != nil {
			return err
		}
	}
	return nil
}

func (h *WaggleDance) ImportArtistList(filename string) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	var artists []string
	for _, l := range strings.Split(string(data), "\n") {
		artists = append(artists, strings.TrimSpace(l))
	}
	strslice.RemoveDuplicates(&artists)

	for _, a := range artists {
		_, isNewArtist, err := h.getArtist(a)
		if err != nil {
			return err
		}
		if isNewArtist {
			logthis.TimedInfo("Artist "+a+" added to DB.", logthis.VERBOSE)
		} else {
			logthis.TimedInfo("Artist "+a+" already was in DB.", logthis.VERBOSE)
		}
	}
	return nil
}

const (
	filterUnmatched = iota
	filterMissingWEB
	filterMissingPerfect
)

func (h *WaggleDance) ExportReleaseList(filename string, filter int) error {
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	// getting all groups
	var artists []Artist
	res := h.db.Order("name collate nocase").Find(&artists)
	if res.Error != nil {
		return res.Error
	}

	for _, a := range artists {
		// getting all unmatched groups from this artist
		var allGroups []Group
		switch filter {
		case filterUnmatched:
			res = h.db.Model(&a).Where("matched = ?", false).Where("ignore = ?", false).Related(&allGroups, "Groups")
		case filterMissingWEB:
			res = h.db.Model(&a).Where("no_webflac = ?", true).Where("ignore = ?", false).Related(&allGroups, "Groups")
		case filterMissingPerfect:
			res = h.db.Model(&a).Where("no_perfect_flac = ?", true).Where("ignore = ?", false).Related(&allGroups, "Groups")
		}
		if res.Error != nil {
			return res.Error
		}

		if len(allGroups) == 0 {
			continue
		}

		if _, err := f.WriteString("# " + a.Name + "\n"); err != nil {
			return err
		}
		for _, g := range allGroups {
			if _, err := f.WriteString(g.ExportString() + "\n"); err != nil {
				return err
			}
		}
	}
	return nil
}
