package main

import (
	"fmt"
	"strconv"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/catastrophic/assistance/ui"
)

func (h *WaggleDance) getArtist(artist string) (Artist, bool, error) {
	var isNewArtist bool
	var a Artist
	// find artist with name in DB
	result := h.db.Where("name = ?", artist).First(&a)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		logthis.TimedInfo(artist+" is a new artist.", logthis.VERBOSE)
		isNewArtist = true

		// if not found:
		searchResults, err := h.d.SearchArtist(artist)
		if err != nil {
			return Artist{}, false, err
		}
		if searchResults.Results.RevisedQuery != "" {
			if !ui.Accept("Deezer suggested: " + ui.RedBold(searchResults.Results.RevisedQuery) + ui.BlueBold(". Accept")) {
				return Artist{}, false, errors.New("deezer suggestion rejected")
			}
			// replacing artist with the deezer suggestion
			artist = searchResults.Results.RevisedQuery
		}
		var artistDeezerID int
		switch searchResults.Results.Artist.Count {
		case 0:
			return Artist{}, false, errors.New("cannot find artist on deezer: " + artist)
		case 1:
			if IsTheSameArtist(artist, searchResults.Results.Artist.Data[0].ArtName) {
				artistDeezerID, _ = strconv.Atoi(searchResults.Results.Artist.Data[0].ArtID)
			} else {
				return Artist{}, false, errors.New("cannot find artist on deezer: " + artist)
			}
		default:
			var candidates []string
			for _, a := range searchResults.Results.Artist.Data {
				candidates = append(candidates, fmt.Sprintf("%s (https://www.deezer.com/artist/%s)", a.ArtName, a.ArtID))
			}

			choice, err := ui.SelectAmongStrictOptions("More than one artist found for "+artist, "Select the correct ID", candidates, nil)
			if err != nil {
				if errors.Is(err, ui.ErrorSkipped) {
					return Artist{}, false, errors.New("Skipping artist " + artist)
				}
				return Artist{}, false, err
			}

			// getting the deezer ID
			for i, a := range candidates {
				if choice == a {
					artistDeezerID, _ = strconv.Atoi(searchResults.Results.Artist.Data[i].ArtID)
					break
				}
			}
		}

		if artistDeezerID == 0 {
			return Artist{}, false, errors.New("could not identify artist")
		}
		// creating Artist with this ID
		a.Name = artist
		a.DeezerID = artistDeezerID
		// checking if the artist is not, after all, already known
		result := h.db.Where("deezer_id = ?", artistDeezerID).First(&a)
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			// saving new artist
			if res := h.db.Save(&a); res.Error != nil {
				return Artist{}, false, res.Error
			}
		} else {
			return a, false, result.Error
		}
	}
	return a, isNewArtist, nil
}

func (h *WaggleDance) getAlbumIDs(artistDeezerID int) []int {
	var allKnownAlbumsIDs []int
	var allKnownAlbums []Group
	if res := h.db.Where("deezer_id = ?", artistDeezerID).Find(&allKnownAlbums); res.Error != nil {
		if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
			logthis.Error(res.Error, logthis.VERBOSEST)
		}
		return allKnownAlbumsIDs
	}

	for _, a := range allKnownAlbums {
		allKnownAlbumsIDs = append(allKnownAlbumsIDs, a.DeezerID)
	}
	return allKnownAlbumsIDs
}
