module waggle

go 1.15

require (
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.5.1
	gitlab.com/catastrophic/assistance v0.40.3
	gitlab.com/passelecasque/obstruction v0.15.9
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v2 v2.2.8
)

// replace gitlab.com/catastrophic/assistance => ../../catastrophic/assistance
// replace gitlab.com/passelecasque/obstruction => ../../passelecasque/obstruction
