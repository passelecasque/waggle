package main

import (
	"regexp"
	"strings"
	"time"

	"gitlab.com/catastrophic/assistance/flac"
	"gitlab.com/catastrophic/assistance/logthis"
)

const (
	regexpFeaturing = `^(?i)(.*) [\(\[](?:feat|ft). (.*?)[\)\]](.*)$`
)

func removeHardCodedFeatArtists(title string) string {
	// sometimes (feat. XXX) is hardcoded into the titles.
	// this removes that part
	r := regexp.MustCompile(regexpFeaturing)
	if r.MatchString(title) {
		hits := r.FindStringSubmatch(title)
		if len(hits) == 4 {
			return hits[1] + hits[3]
		}
	}
	return title
}

func IsTheSameRelease(title1, title2 string, editionNames []string) bool {
	logthis.Info("Comparing "+title1+" vs. "+title2, logthis.VERBOSEST)
	clean1 := flac.OnlyKeepLettersAndNumbers(strings.ToLower(title1))
	clean2 := flac.OnlyKeepLettersAndNumbers(strings.ToLower(title2))
	if clean1 == clean2 || clean1+"ep" == clean2 || clean1 == clean2+"ep" || clean1+"single" == clean2 || clean1 == clean2+"single" {
		return true
	}
	// trying with edition name (ie DeluxeEdition)
	for _, en := range editionNames {
		cleanEdition := flac.OnlyKeepLettersAndNumbers(strings.ToLower(en))
		if clean1 == clean2+cleanEdition || clean1 == clean2+strings.Replace(cleanEdition, "edition", "version", 1) || clean1 == clean2+strings.Replace(cleanEdition, "version", "edition", 1) {
			return true
		}
	}
	if title1 != removeHardCodedFeatArtists(title1) {
		return IsTheSameRelease(removeHardCodedFeatArtists(title1), title2, editionNames)
	}
	return false
}

func IsTheSameArtist(wanted, candidate string) bool {
	logthis.Info("Comparing "+wanted+" vs. "+candidate, logthis.VERBOSEST)
	clean1 := flac.OnlyKeepLettersAndNumbers(strings.ToLower(wanted))
	clean2 := flac.OnlyKeepLettersAndNumbers(strings.ToLower(candidate))
	if clean1 == clean2 || "the"+clean1 == clean2 || clean1 == "the"+clean2 {
		return true
	}
	return false
}

func AboutTheSameNumberOfTracks(target, candidate int, isWEB bool) bool {
	// candidate must have at least as many files as there are tracks
	// candidate can have 2 extra files (cover, one other thing?)
	// TODO maybe be cleverer for singles/EPs with the same name
	numberOfExtraFiles := 2 // cover and maybe something else
	if !isWEB {
		numberOfExtraFiles += 2 // cue+log
	}
	if candidate >= target && candidate <= target+numberOfExtraFiles {
		return true
	}
	return false
}

func IsReleaseDateInTheFuture(date string) bool {
	t, err := time.Parse("2006-01-02", date)
	if err != nil {
		logthis.TimedError(err, logthis.NORMAL)
		// returning true for special highlight
		return true
	}
	now := time.Now()
	return t.After(now)
}
