package main

import (
	"fmt"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
)

const (
	defaultDB = "waggle.db"
)

func main() {
	// parsing CLI
	cli := &waggleArgs{}
	if err := cli.parseCLI(os.Args[1:]); err != nil {
		logthis.TimedError(err, logthis.NORMAL)
		return
	}
	if cli.builtin {
		return
	}
	// loading configuration
	conf, err := NewConfig(DefaultConfigurationFile)
	if err != nil {
		logthis.TimedError(err, logthis.NORMAL)
		return
	}
	// setting up
	wg, err := NewWaggleDance(defaultDB, conf.URL, conf.APIKey, conf.DeezerSID)
	if err != nil {
		logthis.TimedError(err, logthis.NORMAL)
		return
	}
	defer wg.Close()
	// getting known artists
	artists, err := wg.AllArtists()
	if err != nil {
		logthis.TimedError(err, logthis.NORMAL)
		return
	}
	// running subcommands
	switch {
	case cli.showConfig:
		showConfig(conf)
	case cli.check:
		refresh(wg, cli, artists)
	case cli.list:
		list(wg, cli, artists)
	case cli.ignore:
		ignore(wg, cli)
	case cli.match:
		match(wg, cli)
	case cli.unmatch:
		unmatch(wg, cli)
	case cli.addComment:
		addComment(wg, cli)
	case cli.importCmd:
		importCmd(wg, cli)
	case cli.exportCmd:
		exportCmd(wg, cli)
	case cli.remove:
		removeArtist(wg, cli, artists)
	}
}

func showConfig(conf *Config) {
	fmt.Println(conf.String())
	if err := conf.CheckCredentials(userAgent()); err != nil {
		fmt.Println("/!\\ Invalid tracker credentials -- " + err.Error())
	} else {
		fmt.Println("Tracker credentials are valid.")
	}
}

func addComment(wg *WaggleDance, cli *waggleArgs) {
	if err := wg.AddComment(cli.deezerID, cli.userComment); err != nil {
		logthis.TimedError(errors.Wrap(err, "could not save user comment"), logthis.NORMAL)
	} else {
		logthis.TimedInfo("User comment saved.", logthis.NORMAL)
	}
}

func ignore(wg *WaggleDance, cli *waggleArgs) {
	if err := wg.Ignore(cli.deezerID, cli.userComment); err != nil {
		logthis.TimedError(errors.Wrap(err, "could not ignore release"), logthis.NORMAL)
	} else {
		logthis.TimedInfo("Release now set as ignored.", logthis.NORMAL)
	}
}

func match(wg *WaggleDance, cli *waggleArgs) {
	if err := wg.Match(cli.deezerID, cli.trackerID); err != nil {
		logthis.TimedError(errors.Wrap(err, "could not match release"), logthis.NORMAL)
	} else {
		logthis.TimedInfo("Release now set as matched.", logthis.NORMAL)
	}
}

func unmatch(wg *WaggleDance, cli *waggleArgs) {
	if err := wg.UnMatch(cli.deezerID); err != nil {
		logthis.TimedError(errors.Wrap(err, "could not unmatch release"), logthis.NORMAL)
	} else {
		logthis.TimedInfo("Release now set as unmatched.", logthis.NORMAL)
	}
}

func refresh(wg *WaggleDance, cli *waggleArgs, artists []Artist) {
	if cli.artist != "" {
		if err := wg.RefreshArtist(cli.artist); err != nil {
			logthis.TimedError(err, logthis.NORMAL)
		}
	} else {
		for _, a := range artists {
			if err := wg.RefreshArtist(a.Name); err != nil {
				logthis.TimedError(err, logthis.NORMAL)
			}
		}
	}
}

func list(wg *WaggleDance, cli *waggleArgs, artists []Artist) {
	var found bool
	for _, a := range artists {
		if cli.artist != "" && a.Name != cli.artist {
			continue
		}
		if err := wg.ShowArtist(a.Name, cli.unMatched, cli.listIgnored, cli.missingPerfect, cli.missingWEB); err != nil {
			logthis.TimedError(err, logthis.NORMAL)
		}
		found = true
	}
	if !found {
		logthis.TimedError(errors.New("no matching artist found"), logthis.NORMAL)
	}
}

func importCmd(wg *WaggleDance, cli *waggleArgs) {
	switch {
	case cli.userValues:
		if err := wg.ImportFromCSV(cli.targetFile); err != nil {
			logthis.TimedError(errors.Wrap(err, "could not import from csv"), logthis.NORMAL)
		} else {
			logthis.TimedInfo("User content from csv file was imported into the current DB.", logthis.NORMAL)
		}
	case cli.artistsList:
		if err := wg.ImportArtistList(cli.targetFile); err != nil {
			logthis.TimedError(errors.Wrap(err, "could not import from artist file"), logthis.NORMAL)
		} else {
			logthis.TimedInfo("The artist list was imported into the current DB.", logthis.NORMAL)
		}
	}
}

func exportCmd(wg *WaggleDance, cli *waggleArgs) {
	switch {
	case cli.userValues:
		if err := wg.ExportToCSV(cli.targetFile); err != nil {
			logthis.TimedError(errors.Wrap(err, "could not export to csv"), logthis.NORMAL)
		} else {
			logthis.TimedInfo("User content from DB was exported to a csv file.", logthis.NORMAL)
		}
	case cli.artistsList:
		if err := wg.ExportArtistList(cli.targetFile); err != nil {
			logthis.TimedError(errors.Wrap(err, "could not export artist list"), logthis.NORMAL)
		} else {
			logthis.TimedInfo("The list of artists found in the DB was exported to a file.", logthis.NORMAL)
		}
	case cli.unMatched:
		if err := wg.ExportReleaseList(cli.targetFile, filterUnmatched); err != nil {
			logthis.TimedError(errors.Wrap(err, "could not export unmatched releases list"), logthis.NORMAL)
		} else {
			logthis.TimedInfo("The list of unmatched releases found in the DB was exported to a file.", logthis.NORMAL)
		}
	case cli.missingWEB:
		if err := wg.ExportReleaseList(cli.targetFile, filterMissingWEB); err != nil {
			logthis.TimedError(errors.Wrap(err, "could not export list of releases missing web FLACs"), logthis.NORMAL)
		} else {
			logthis.TimedInfo("The list of releases missing web FLACs found in the DB was exported to a file.", logthis.NORMAL)
		}
	case cli.missingPerfect:
		if err := wg.ExportReleaseList(cli.targetFile, filterMissingPerfect); err != nil {
			logthis.TimedError(errors.Wrap(err, "could not export list of releases missing perfect FLACs"), logthis.NORMAL)
		} else {
			logthis.TimedInfo("The list of releases missing perfect FLACs found in the DB was exported to a file.", logthis.NORMAL)
		}
	}
}

func removeArtist(wg *WaggleDance, cli *waggleArgs, artists []Artist) {
	var found bool
	var err error
	for _, a := range artists {
		if a.Name == cli.artist {
			if wg.db.Delete(&a).Error != nil {
				logthis.TimedError(errors.Wrap(err, "error removing artist from DB"), logthis.NORMAL)
			}
			found = true
		}
	}
	if found {
		logthis.TimedInfo("Artist removed from DB.", logthis.NORMAL)
	} else {
		logthis.TimedError(errors.New("could not find artist in DB"), logthis.NORMAL)
	}
}
