GO = GO111MODULE=on go
VERSION=`git describe --tags`

all: fmt check test-coverage build

prepare:
	${GO} get -u github.com/divan/depscheck
	${GO} get github.com/warmans/golocc
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.47.2

deps:
	${GO} mod download

fmt:
	${GO} fmt ./...

check: fmt
	golangci-lint run --timeout=5m

info: fmt
	depscheck -totalonly -tests .
	golocc .

test-coverage:
	${GO} test -race -coverprofile=coverage.txt -covermode=atomic ./...

clean:
	rm -f waggle
	rm -f waggle_windows.exe
	rm -f waggle_darwin
	rm -f waggle_darwin_arm64
	rm -f coverage.txt

build:
	${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o waggle
	GOOS=windows GOARCH=amd64 ${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o waggle_windows.exe
	GOOS=darwin GOARCH=amd64 ${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o waggle_darwin
	GOOS=darwin GOARCH=arm64 ${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o waggle_darwin_arm64

install:
	${GO} install -trimpath -ldflags "-X main.Version=${VERSION}"




