package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig(t *testing.T) {
	fmt.Println("+ Testing Config...")
	check := assert.New(t)

	// setting up
	c := &Config{}
	err := c.Load("test/config.yaml")
	check.Nil(err)

	// general
	fmt.Println("Checking general")
	check.Equal(0, c.LogLevel)

	// trackers
	fmt.Println("Checking trackers")
	check.Equal("secretapikey", c.APIKey)
	check.Equal("https://blue.it", c.URL)
	// metadata
	fmt.Println("Checking metadata")
	check.Equal("deezersidcookie90aedfbc323", c.DeezerSID)

	fmt.Println(c.String())
}
