package main

import (
	"io/ioutil"
	"strconv"
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/passelecasque/obstruction/tracker"
	yaml "gopkg.in/yaml.v2"
)

var config *Config
var onceConfig sync.Once

const (
	errorReadingConfig = "could not read configuration file"
	errorLoadingYAML   = "could not decode yaml"

	DefaultConfigurationFile = "config.yaml"
)

type Config struct {
	LogLevel         int    `yaml:"log_level"`
	APIKey           string `yaml:"api_key"`
	URL              string `yaml:"tracker_url"`
	DeezerSID        string `yaml:"deezer_sid"`
	DeezerConfigured bool
}

func NewConfig(path string) (*Config, error) {
	var newConfigErr error
	onceConfig.Do(func() {
		newConf := &Config{}
		if err := newConf.Load(path); err != nil {
			newConfigErr = err
			return
		}
		// set the global pointer once everything is OK.
		config = newConf
	})
	return config, newConfigErr
}

func (c *Config) String() string {
	txt := "Waggle configuration:\n"
	txt += "\tLog level: " + strconv.Itoa(c.LogLevel) + "\n"
	txt += "\tAPI Key: " + c.APIKey + "\n"
	txt += "\tURL: " + c.URL + "\n"
	txt += "\tDeezer SID: " + c.DeezerSID + "\n"
	return txt
}

func (c *Config) Load(file string) error {
	// loading the configuration file
	b, err := ioutil.ReadFile(file)
	if err != nil {
		return errors.Wrap(err, errorReadingConfig)
	}
	return c.LoadFromBytes(b)
}

func (c *Config) LoadFromBytes(b []byte) error {
	err := yaml.Unmarshal(b, &c)
	if err != nil {
		return errors.Wrap(err, errorLoadingYAML)
	}
	return c.check()
}

func (c *Config) check() error {
	if err := logthis.CheckLevel(c.LogLevel); err != nil {
		return err
	}
	// setting log level
	logthis.SetLevel(c.LogLevel)
	logthis.SetStdOutput(true)

	// tracker checks
	if c.APIKey == "" {
		logthis.Info("Missing log in information (API key)", logthis.VERBOSEST)
	}
	if c.URL == "" {
		return errors.New("Missing tracker URL")
	}
	// metadata checks
	if c.DeezerSID == "" {
		return errors.New("A valid Deezer SID must be provided")
	}
	return nil
}

// CheckCredentials separately, to find out if the API Key is invalid or the session cookie out of date.
func (c *Config) CheckCredentials(userAgent string) error {
	// checking API Key
	tWithKey, err := tracker.NewGazelle("gazelle_tracker", c.URL, "", "", "", "", c.APIKey, userAgent)
	if err != nil {
		return err
	}
	tWithKey.StartRateLimiter()
	if err := tWithKey.Login(); err != nil {
		return errors.Wrap(err, "invalid API key")
	}
	return nil
}
